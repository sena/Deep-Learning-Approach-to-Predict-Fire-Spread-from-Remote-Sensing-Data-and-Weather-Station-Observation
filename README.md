
***

# Deep Learning Approach to Predict Fire Spread From Remote Sensing Data And Weather Station Observation
This study aims to build and assess the performance of various DL approaches for predicting wildfire spread with the parameter of time and space evolution along with lateral fire progression.  DEM (Digital Elevation Model) represents the topography, vegetation, or fuel simulated by land cover and weather information provided by the meteorological stations was combined to build historical archives for training the models. 

Prediction of the next fire spread within two days should be long enough to let the fire disappear by itself or let people be aware of fires and be able to mitigate the danger. In this study, the remote sensing and weather information datasets have been used to train three types of DL networks LSTM (Long Short-Term Memory), RNN (Recurrent Neural Network) and GRU (Gated Recurrent Unit).

![intro](documentation/image/1.1.png)

## Content

- [Overview](#overview)
- [Getting Started](#getting-started)
- [Description](#description)
- [Data Processing](#data-processing)
    - [Fire Dataset](#fire-dataset)
    - [Land Cover](#land-cover)
    - [Temperature](#temperature)
    - [Wind](#wind)
    - [Elevation](#elevation)
    - [Precipitation & Pressure](#precipitation-pressure)
- [Model Development](#model-development)
- [Model Deployment](#model-deployment)
- [References](#references)



## Overview

This study concentrates on models that can predict the location of fires up to one day
in advance. Due to limited time and resources, other aspects of the fire, impact, and
shape, cannot be predicted. RNN, LSTM, and GRU are the DL algorithms that will be
investigated.

## Getting Started
The code was created in Python 3.10.12 and executed in a Google collab (Jupyter Notebook). For local implementation,please ensure pip is set up and all necessary packages are installed:

- basemap
- matplotlib.cm
- geopandas
- numpy 
- datetime
- dateutil
- seaborn
- copy
- itertools
- random
- os
- math
- ee
- time
- geemap
- glob
- MetPy
- re
- pyhdf
- numpy
- scipy
- pandas
- deepgraph
- geopandas
- shapely
- sklearn
- palettable
- keras
- tensorflow

## Description

The project successfully worked properly and met the expectations. Data processing, DL model development, and DL Model deployment are the three approaches applied in this study. These approaches are based on deep learning workflow
to address real-world challenges, as stated in Section 2.5. The first step, which is data
processing includes the implementation of data collection, data extraction, data preprocessing, and feature engineering. The second step is model development, where three LSTM, RNN, and GRU algorithms were applied to learn patterns, including performing the hyperparameter tuning process from the data provided in the first step. The last component
is to deploy and predict the real fire case to evaluate the results of the quantitatively
three deep learning algorithms and visually monitor the quality of the models and the
combination of distance and direction models to produce the next day fire location from
real fire samples.

## Data Processing
The approaches included collection and extraction from various sources and combined
them into the same format dataset, preprocessing data, and feature engineering, which
involves transformation and calculation into usable data. All this process is necessary
to provide reliable and useful data for the model direction and distance development.


### Data Extraction

#### Fire Dataset

#### Land Cover
Data :
- MODIS Land Cover Type Yearly Global 500m

Data type :
- ee.ImageCollection MCD12Q1.061 


Dataset Provider :
NASA LP DAAC at the USGS EROS Center
https://lpdaac.usgs.gov/products/mcd12q1v061/

UserGuide :
https://lpdaac.usgs.gov/documents/101/MCD12_User_Guide_V6.pdf

This program included :
- Extract Land Cover Type 1: Annual International Geosphere-Biosphere Programme (IGBP) classification with resolution 500 Meters.

#### Temperature
Data :
- Noaa land based weather station daily summaries
- Aqua Land Surface Temperature and Emissivity 8-Day 
- Climate Reanalysis Produced by ECMWF / Copernicus Climate Change Service
- Terra Land Surface Temperature and Emissivity Daily Global 1km

Data type :
- CSV land station weather daily summaries
- ee.ImageCollection MYD11A2.061 
- ee.ImageCollection ERA5 Daily Aggregates 
- ee.ImageCollection MOD11A1.061 

Bands :
- The nearest weather station
- LST_Day_1km (Daytime Land Surface Temperature)
- mean_2m_air_temperature (Average air temperature at 2m height (daily average))

Dataset Provider :
- https://lpdaac.usgs.gov/products/mcd12q1v061/
- https://developers.google.com/earth-engine/datasets/catalog/MODIS_061_MYD11A2
- https://www.ncei.noaa.gov/
- https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_DAILY#bands
- https://developers.google.com/earth-engine/datasets/catalog/MODIS_061_MOD11A1


This program included :
- Find nearest Land Based Station Location with the location of fire.
- Extract NOAA API Database for land based station temperature dataset (daily-summaries).
- Extract TERRA Remote Sensing database for missing temperature dataset.
- Extract AQUA Remote Sensing database for missing temperature dataset.
- Extract ERA5 Weather Reanalisis database for missing temperature dataset.
- Standardize all diferent units such as convert the temperature from Kelvin to Celsius.

#### Wind

Data :
- Climate Reanalysis Produced by ECMWF / Copernicus Climate Change Service

Data type :
- ee.ImageCollection ERA5 Daily Aggregates 

Bands :
- u_component_of_wind_10m (10m u-component of wind (daily average))
- v_component_of_wind_10m (10m v-component of wind (daily average))

Dataset Provider :
- https://lpdaac.usgs.gov/products/mcd12q1v061/
- https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_DAILY#bands

This program included :
- Extract ERA5 Weather Reanalisis database for  U and V component dataset.
- Calculate Wind Direction and speed from U and V component.

#### Elevation

Data :
- NASA SRTM Digital Elevation 30m

Data type :
- ee.Image USGS/SRTMGL1_003

Bands :
- elevation (30 meters Elevation)

Dataset Provider :
- https://lpdaac.usgs.gov/products/mcd12q1v061/
- https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_DAILY#bands

This program included :
- Extract elevation of the individual fire location.

#### Precipitation & Pressure


Data :
- Climate Reanalysis Produced by ECMWF / Copernicus Climate Change Service

Data type :
- ee.ImageCollection ERA5 Daily Aggregates  27830 meters Resolution

Bands :
- total_precipitation (Total precipitation (daily sums)(m))
- surface_pressure (Surface pressure (daily average)(Pa))

Dataset Provider :
- https://lpdaac.usgs.gov/products/mcd12q1v061/
- https://developers.google.com/earth-engine/datasets/catalog/ECMWF_ERA5_DAILY#bands

This program included :
- Extract ERA5 Weather Reanalisis database for precipitation of individual fire respective spatial.
- Extract ERA5 Weather Reanalisis database for pressure of individual fire respective spatial.

### Features Engineering

- Latitude and Longitude
- Clustering Fire Event
- Euclidean Space
- Distance
- Direction

## Model Development

Train 2 models, which are direction and distance, use three types of deep learning methods, especially sequence and time series methods.

- LSTM
- RNN
- GRU


## Model Deployment

Calculate the next direction and distance to calculate the next tractory or coordinate point of the fire.

### Running
The project successfully trains a time series model with several approaches (LSTM, RNN, GRU) and provides graphing of the trajectories, specifically the latitude and longitude coordinates. 
